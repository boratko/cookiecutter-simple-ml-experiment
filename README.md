# Cookiecutter: Simple ML Research
> A simple [Cookiecutter](https://github.com/cookiecutter/cookiecutter) template for ML research packages

To use, first install Cookiecutter and then run
```
cookiecutter https://gitlab.com/boratko/cookiecutter-simple-ml-experiment
```

# Template Details
The aim of this template is to provide conventions and structure which facilitates reproducible ML research. Much of this simply comes down to adhering to good software development practices. This includes:

## Packaging
You should install your project as an editable Python package. Here are some benefits of installing as a package:
1. Import from any location
2. Easier to test
3. Easily published / shared between projects

This template uses the [recommended packaging practices described by setuptools](https://setuptools.readthedocs.io/en/latest/userguide/quickstart.html), which includes:
- `pyproject.toml` specifying build requirements
- `setup.cfg` for setuptools configuration
- `setup.py` shim to enable editable installation

## Testing
It is strongly recommended to test your code, even if it is simply to make sure that you can make it through a single training loop without encountering syntax errors. [Pytest](https://docs.pytest.org/en/stable/) is the recommended testing framework, which allows for quickly writing small tests without sacrificing complexity when it is required.

## Command Line
Ideally, your code is usable as both a library (eg. for importing in notebooks) and via a command line front-end. To make this 
