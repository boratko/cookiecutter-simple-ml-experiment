# {{cookiecutter.project_name}}
> {{cookiecutter.description}}

## Dev Instructions
```
git clone {repo-url} --recurse-submodules
pip install -e {{cookiecutter.directory_name}}
```
## Usage
This package will install a command accessible as follows:
```
{{cookiecutter.directory_name}} --help
```
(You can also access this via the script in `/bin/{{cookiecutter.directory_name}}`, which can be useful to provide a "handle" for debuggers, for instance.)
