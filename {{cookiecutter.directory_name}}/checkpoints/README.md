# {{cookiecutter.project_name}} Checkpoints
This directory houses model checkpoints. If committed to Git, it is recommended to use [Git LFS](https://git-lfs.github.com/).