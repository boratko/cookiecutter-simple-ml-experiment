# {{cookiecutter.project_name}} Lib
This directory should store *versioned* dependencies. The most common use-case for this folder it so include python dependencies as [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
