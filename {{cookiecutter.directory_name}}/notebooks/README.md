# {{cookiecutter.project_name}} Notebooks
This directory includes relevant notebooks. It is recommended to store notebooks in Git using [jupytext](https://github.com/mwouts/jupytext) for clean diffs.