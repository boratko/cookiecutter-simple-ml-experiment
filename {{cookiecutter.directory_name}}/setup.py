"""
{{cookiecutter.description}}
"""
import fastentrypoints  # modifies setuptools to make command line interaction faster
import setuptools

setuptools.setup()
