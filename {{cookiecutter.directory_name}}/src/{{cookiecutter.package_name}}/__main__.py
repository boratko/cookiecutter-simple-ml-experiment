# This file allows you to execute the module as a script using `python -m {{cookiecutter.package_name}}`
from .main import app

app(prog_name="{{cookiecutter.package_name}}")
