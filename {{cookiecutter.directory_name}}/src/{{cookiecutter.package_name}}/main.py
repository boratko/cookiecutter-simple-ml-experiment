import typer
from loguru import logger

app = typer.Typer(help="{{cookiecutter.description}}")


@app.command()
def main():
    logger.info("Use this instead of print to record relevant info to console")
