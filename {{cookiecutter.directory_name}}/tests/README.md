# {{cookiecutter.project_name}} Tests
This folder stores the tests for the project. It is recommended to use [pytest](https://docs.pytest.org/en/stable/) for testing.